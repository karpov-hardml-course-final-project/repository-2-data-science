import tensorflow as tf
import tensorflow_hub as hub

import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

with tf.Graph().as_default():
  # for some reason v3 or v4 doesn't work as tfhub model ins't foun in /tmp
  module = hub.Module("https://tfhub.dev/google/universal-sentence-encoder/2")
  text = tf.placeholder(tf.string, [None])
  embedding = module(text)

  init_op = tf.group([tf.global_variables_initializer(), tf.tables_initializer()])
  with tf.Session() as session:
    session.run(init_op)
    tf.saved_model.simple_save(
        session,
        "./use/2",
        inputs = {"text": text},
        outputs = {"embedding": embedding},
        legacy_init_op = tf.tables_initializer()        
    )

