import pickle
import numpy as np

class PrecalcClusters():

    def __init__(self, path: str) -> None:
        with open(path, 'rb') as f:
            clusters = pickle.load(f)
        self.num_clusters = len(clusters)
        if self.num_clusters == 0:
            raise ValueError("zero clusters load!")

        self.dim = clusters[list(clusters.keys())[0]].shape[0]
        self.centers = np.zeros((self.num_clusters, self.dim))
        self.labels = []
        for ind, k in enumerate(clusters.keys()):
            self.centers[ind] = clusters[k]
            self.labels.append(k)

    def predict_label(self, embedding: np.array) -> str:
        if embedding.shape[0] != self.dim:
            raise ValueError("incorrect input size")
        return self.labels[np.argmin(np.linalg.norm(self.centers - embedding, axis=1))]


        
