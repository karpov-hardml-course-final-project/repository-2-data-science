import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='qatoolbox',
    version='0.0.1a1',
    author='Andrey Poletaev',
    author_email='poletaev.andrew@gmail.com',
    description='QA models for clustering and knn-indexing',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/karpov-hardml-course-final-project/repository-2-data-science',
    project_urls = {
        "Bug Tracker": "https://gitlab.com/karpov-hardml-course-final-project/repository-2-data-science/issues"
    },
    packages=['qatoolbox'],
    install_requires=['numpy'],
)